const express = require('express')
const routerStudent = require('./routes/student')

const app = express();

//provide the parse
app.use(express.json())


// use routerStudent
app.use('/student',routerStudent);

app.listen(4000,()=>{
    console.log("Server started on Port 4000");
});
