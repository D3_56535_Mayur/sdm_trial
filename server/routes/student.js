const { request, response } = require('express');
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router();

// get all students
router.get('/', (request, response) => {

   // const connection = db.openConnection();

    const query = `select * from student`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
      })
});

// //Admission of student
// router.post('/admission', (request, response) => {

//     const connection = db.openConnection();

//     const { name, classOfStudent, division, dateofbirth, parentMobileNo } = request.body;

//     const statement = `insert into student values(default,'${name}','${classOfStudent}',
//                         '${division}','${dateofbirth}',${parentMobileNo})`;

//     connection.query(statement, (error, result) => {
//         connection.end();
//         response.send(utils.createResult(error, result));
//     });
// });

// //get student by id
// router.get('/:rollNo', (request, response) => {
//     console.log('Hello');
//     const connection = db.openConnection();
//     const {rollNo} = request.params;
//     const statement = `select * from student where roll_no = ${rollNo}`

//     connection.query(statement, (error, result) => {
//         connection.end();
//         response.send(utils.createResult(error, result))
//     });
// });

// // Edit specific Student's class and division
// router.patch('/edit/:rollNo',(request, response) => {
//     const connection = db.openConnection();

//     const {rollNo} = request.params;

//     const{classOfStudent,division} = request.body;

//     const statement = `update student set class = '${classOfStudent}', division =' ${division}' where roll_no = ${rollNo}`

//     connection.query(statement,(error,result)=>{
//         connection.end();
//         response.send(utils.createResult(error,result));
//     });

// });

// // Delete Student
// router.delete('/delete/:rollNo',(request,response)=>{
//     const connection = db.openConnection();

//     const {rollNo} = request.params;

//     const statement = `delete from student where roll_no = ${rollNo}`

//     connection.query(statement,(error,result)=>{
//         connection.end();
//         response.send(utils.createResult(error,result));
//     });

// });

// // Fetch all students of particular class
// router.get('/getByClass/:classOfStudent',(request,response)=>{

//     const connection = db.openConnection();

//     const {classOfStudent} = request.params;

//     const statement = `select * from student where class = '${classOfStudent}'`

//     connection.query(statement,(error,result)=>{
//         connection.end();
//         response.send(utils.createResult(error,result));
//     });

// });

// // Fetch all students of particular birth year
// router.get('/studentByYear',(request,response)=>{
//     const connection = db.openConnection();
    
//     const {year} = request.query;
//     console.log(year);
//     const statement = `select * from student where year(dateofbirth)='${year}'`

//     connection.query(statement,(error,result)=>{
//         response.send(utils.createResult(error,result))
//     });
// });

 module.exports = router